-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 07, 2019 at 07:53 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rens_naivebayes`
--

-- --------------------------------------------------------

--
-- Table structure for table `parameters`
--

CREATE TABLE `parameters` (
  `id` int(11) NOT NULL,
  `parameter` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `parameters`
--

INSERT INTO `parameters` (`id`, `parameter`) VALUES
(1, 'week'),
(2, 'weather'),
(3, 'holiday'),
(4, 'month');

-- --------------------------------------------------------

--
-- Table structure for table `parameters_values`
--

CREATE TABLE `parameters_values` (
  `id` int(11) NOT NULL,
  `id_parameter` int(11) NOT NULL,
  `value` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `parameters_values`
--

INSERT INTO `parameters_values` (`id`, `id_parameter`, `value`) VALUES
(1, 1, 'weekend'),
(2, 1, 'weekday'),
(3, 2, 'sunny'),
(4, 2, 'rainy'),
(5, 3, 'yes'),
(6, 3, 'no'),
(7, 4, 'early'),
(8, 4, 'end');

-- --------------------------------------------------------

--
-- Table structure for table `training_set`
--

CREATE TABLE `training_set` (
  `no` int(11) NOT NULL,
  `week` enum('weekday','weekend') NOT NULL,
  `weather` enum('rainy','sunny') NOT NULL,
  `holiday` enum('yes','no') NOT NULL,
  `month` enum('early','end') NOT NULL,
  `sales` enum('crowded','quiet') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `training_set`
--

INSERT INTO `training_set` (`no`, `week`, `weather`, `holiday`, `month`, `sales`) VALUES
(1, 'weekend', 'sunny', 'no', 'early', 'crowded'),
(2, 'weekend', 'rainy', 'no', 'end', 'quiet'),
(3, 'weekday', 'sunny', 'no', 'early', 'crowded'),
(4, 'weekday', 'sunny', 'yes', 'end', 'quiet'),
(5, 'weekend', 'rainy', 'yes', 'early', 'quiet'),
(6, 'weekday', 'rainy', 'no', 'early', 'quiet'),
(7, 'weekday', 'sunny', 'no', 'end', 'crowded'),
(8, 'weekend', 'sunny', 'yes', 'end', 'crowded'),
(9, 'weekend', 'rainy', 'no', 'early', 'quiet'),
(10, 'weekday', 'sunny', 'no', 'early', 'quiet'),
(11, 'weekday', 'sunny', 'yes', 'end', 'crowded'),
(12, 'weekend', 'rainy', 'no', 'end', 'crowded'),
(13, 'weekend', 'sunny', 'yes', 'early', 'quiet'),
(14, 'weekday', 'rainy', 'yes', 'end', 'quiet'),
(15, 'weekend', 'rainy', 'yes', 'end', 'crowded'),
(16, 'weekend', 'rainy', 'yes', 'end', 'quiet');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `parameters`
--
ALTER TABLE `parameters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `parameters_values`
--
ALTER TABLE `parameters_values`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_parameter` (`id_parameter`);

--
-- Indexes for table `training_set`
--
ALTER TABLE `training_set`
  ADD PRIMARY KEY (`no`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `parameters`
--
ALTER TABLE `parameters`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `parameters_values`
--
ALTER TABLE `parameters_values`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `training_set`
--
ALTER TABLE `training_set`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `parameters_values`
--
ALTER TABLE `parameters_values`
  ADD CONSTRAINT `parameters_values_ibfk_1` FOREIGN KEY (`id_parameter`) REFERENCES `parameters` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
