<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url_helper');
		$this->load->model('rens');
	}

	public function index()
	{
		// $data['form']	=	$this->rens->getData();
		// print_r($data['form']);
		$this->load->view('index');
	}

	public function proses()
	{
		$data_test = array(
			'weektest' 		=> $this->input->post('week'), 
			'weathertest' 	=> $this->input->post('weather'), 
			'holidaytest' 	=> $this->input->post('holiday'), 
			'monthtest' 	=> $this->input->post('month') 	
		);
		// print_r($data_test); die;
		// prior
		$priorCrowded	= $this->rens->getPriorCrowded();
		$priorQuiet		= $this->rens->getPriorQuiet();

		//evidence	- week
		$WeekendCrowded	= $this->rens->getWeekendCrowded();
		$eviWeekendCrowded = $WeekendCrowded->result/$priorCrowded->result;

		$WeekendQuiet	= $this->rens->getWeekendQuiet();
		$eviWeekendQuiet = $WeekendQuiet->result/$priorQuiet->result;

		$WeekdayCrowded	= $this->rens->getWeekdayCrowded();
		$eviWeekdayCrowded = $WeekdayCrowded->result/$priorCrowded->result;

		$WeekdayQuiet	= $this->rens->getWeekdayQuiet();
		$eviWeekdayQuiet = $WeekdayQuiet->result/$priorQuiet->result;

		//evidence	- weather
		$SunnyCrowded	= $this->rens->getSunnyCrowded();
		$eviSunnyCrowded = $SunnyCrowded->result/$priorCrowded->result;

		$SunnyQuiet	= $this->rens->getSunnyQuiet();
		$eviSunnyQuiet = $SunnyQuiet->result/$priorQuiet->result;

		$RainyCrowded	= $this->rens->getRainyCrowded();
		$eviRainyCrowded = $RainyCrowded->result/$priorCrowded->result;

		$RainyQuiet	= $this->rens->getRainyQuiet();
		$eviRainyQuiet = $RainyQuiet->result/$priorQuiet->result;

		//evidence	- holiday
		$YesCrowded	= $this->rens->getYesCrowded();
		$eviYesCrowded = $YesCrowded->result/$priorCrowded->result;

		$YesQuiet	= $this->rens->getYesQuiet();
		$eviYesQuiet = $YesQuiet->result/$priorQuiet->result;

		$NoCrowded	= $this->rens->getNoCrowded();
		$eviNoCrowded = $NoCrowded->result/$priorCrowded->result;

		$NoQuiet	= $this->rens->getNoQuiet();
		$eviNoQuiet = $NoQuiet->result/$priorQuiet->result;

		//evidence	- month
		$EarlyCrowded	= $this->rens->getEarlyCrowded();
		$eviEarlyCrowded = $EarlyCrowded->result/$priorCrowded->result;

		$EarlyQuiet	= $this->rens->getEarlyQuiet();
		$eviEarlyQuiet = $EarlyQuiet->result/$priorQuiet->result;

		$EndCrowded	= $this->rens->getEndCrowded();
		$eviEndCrowded = $EndCrowded->result/$priorCrowded->result;

		$EndQuiet	= $this->rens->getEndQuiet();
		$eviEndQuiet = $EndQuiet->result/$priorQuiet->result;

		// print_r($eviEarlyCrowded); die;

		//likelihood - crowded
		if ($data_test['weektest'] == 'weekend'){
			$likeWeek = $eviWeekendCrowded;
		}else
		{
			$likeWeek = $eviWeekdayCrowded;
		}
		
		if ($data_test['weathertest'] == 'sunny'){
			$likeWeather = $eviSunnyCrowded;
		}else
		{
			$likeWeather = $eviRainyCrowded;
		}

		if ($data_test['holidaytest'] == 'yes'){
			$likeHoliday = $eviYesCrowded;
		}else
		{
			$likeHoliday = $eviNoCrowded;
		}

		if ($data_test['monthtest'] == 'early'){
			$likeMonth = $eviEarlyCrowded;
		}else
		{
			$likeMonth = $eviEndCrowded;
		}

		$totLikelihoodCrowded	= $likeWeek * $likeWeather * $likeHoliday * $likeMonth;

		//likelihood - quiet
		if ($data_test['weektest'] == 'weekend'){
			$likeWeek = $eviWeekendQuiet;
		}else
		{
			$likeWeek = $eviWeekdayQuiet;
		}
		
		if ($data_test['weathertest'] == 'sunny'){
			$likeWeather = $eviSunnyQuiet;
		}else
		{
			$likeWeather = $eviRainyQuiet;
		}

		if ($data_test['holidaytest'] == 'yes'){
			$likeHoliday = $eviYesQuiet;
		}else
		{
			$likeHoliday = $eviNoQuiet;
		}

		if ($data_test['monthtest'] == 'early'){
			$likeMonth = $eviEarlyQuiet;
		}else
		{
			$likeMonth = $eviEndQuiet;
		}

		$totLikelihoodQuiet	= $likeWeek * $likeWeather * $likeHoliday * $likeMonth;
		// echo $totLikelihoodQuiet; die;

		// Result //
		$resultCrowded 	= $totLikelihoodCrowded * $priorCrowded->result;
		$resultQuiet 	= $totLikelihoodQuiet * $priorQuiet->result;

		if ($resultCrowded >= $resultQuiet) { 
			echo ("<script LANGUAGE='JavaScript'>
			window.alert('Prediksi penjualan hari ini : RAMAI');
			window.location.href='..';
			</script>");
		}else{ 
			echo ("<script LANGUAGE='JavaScript'>
			window.alert('Prediksi penjualan hari ini : SEPI');
			window.location.href='..';
			</script>");
		}
	}

}
