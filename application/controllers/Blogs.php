<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blogs extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    $this->load->model('blogs_model');
    $this->load->helper('url_helper');
  }

	public function index()
	{
    $data['blogs'] = $this->blogs_model->get();
		$this->load->view('components/header');
    $this->load->view('blogs/index',$data);
    $this->load->view('components/footer');
	}

}
