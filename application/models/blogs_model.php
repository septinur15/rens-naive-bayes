<?php
class Blogs_model extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    $this->load->database();
  }

  public function get()
  {
    $query = $this->db->get('blogs');

    return $query->result_array();
  }

}

 ?>
