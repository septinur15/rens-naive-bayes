<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'welcome';
$route['api/get'] = 'api_server/get';
$route['blogs'] = 'blogs';
$route['welcome'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE; 
